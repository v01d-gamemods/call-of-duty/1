from trainerbase.common.teleport import Teleport, Vector3

from objects import player_x, player_y, player_z


tp = Teleport(player_x, player_y, player_z, dash_coefficients=Vector3(100, 100, 100), minimal_movement_vector_length=5)
