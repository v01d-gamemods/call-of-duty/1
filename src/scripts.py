from functools import partial

from trainerbase.common.helpers import regenerate
from trainerbase.scriptengine import ScriptEngine

from memory import CODGameX86Address
from objects import hp, max_hp_stub


regen_script_engine = ScriptEngine(delay=0.1)
update_dll_address_script_engine = ScriptEngine(delay=5)

regenerate_hp = regen_script_engine.simple_script(
    partial(regenerate, hp, max_hp_stub, percent=1, min_value=1),
)

update_dll_address = update_dll_address_script_engine.simple_script(
    CODGameX86Address.update_dll_base_address,
    enabled=True,
)
