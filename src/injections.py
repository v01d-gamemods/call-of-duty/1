from typing import override

from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.common.helpers import suppress_memory_exceptions

from memory import CODGameX86Address


class CODCodeInjection(CodeInjection):
    @override
    @suppress_memory_exceptions
    def eject(self):
        return super().eject()


class CODAllocatingCodeInjection(AllocatingCodeInjection):
    @override
    @suppress_memory_exceptions
    def eject(self):
        return super().eject()


rapid_fire = CODCodeInjection(
    CODGameX86Address(0xD064),
    b"\x90" * 3,
)

infinite_ammo = CODAllocatingCodeInjection(
    CODGameX86Address(0xD2E2),
    """
        mov dword [eax + ecx * 4 + 0x1F4], 999
    """,
    original_code_length=7,
)
